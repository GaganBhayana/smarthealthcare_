### Introduction:

In the current scenario the management system used in the healthcare sector is very inefficient. There is no organised procedure to handle the patient’s data, also the current methods waste a lot of time and paper. So, to improve upon these problems our system focuses on the above-mentioned aspects and tries to rectify while taking possible measures.

### Main idea:
Our system will help the patients to register themselves on the platform and using our recommendation system the patient can get a basic idea of the possible diseases which will be predicted based on the symptoms provided by the user. 

Now our system will recommend the nearest doctors who is specialize in the required category of diseases. From the list of doctors, user will choose a doctor and will have to take an appointment for that doctor. Every doctor is a part of some hospital registered on our platform. So, when user will take the appointment, a QR code would be generated and his entry will be stored in the appointments list of the respective hospital.

Now on the day of appointment the patient will reach the hospital and scan his QR code, based on which he would get an appointment number. When the user scans his QR code, if he was a returning customer then his past records would reach the doctor with his appointment details. The past records of the patient are only available to that doctor for which user is registered.

When the user would reach the doctor he would only have to give this appointment number to the doctor who can then see all the details of the user and his past records.

Every time a patient becomes ill then he would not have to enter all his details again which would same time. If the patient is already a registered user of our system then he only need to take the appointment from the required doctor.

So, this system would save time of standing in line and taking the appointment and also it would save paper since every thing is being done via electronic devices. The system would be most beneficial when the patient switches from one hospital to another, since all the records and patient details are maintained on a centralized server, therefore the other hospital will directly fetch the details from our database.


### Features:

- Recommendation System

    This module would help the patient to identify the diseases they are suffering from based on the symptoms observed by them. The system will provide them with a list of probable diseases and the specialist doctors in those fields. The patient can then filter those results based on his/her preferences (example Location, ratings, price etc) and choose the physician they want to book an appointment with.

 

- Online Appointment

    With the help of this module the patients can book appointment with the doctor on a date of their choice. Upon successful payment they will be provided with a QR code that will consist encoded information about the patient and the attending doctor.

 

- QR Code Scanner at the Hospital

    The hospital will consist of QR code scanner instead of normal receptions. The scanner will scan the QR code of the patient and provide them with the e-token and at the same time all the patients previous reports would be delivered to the respective doctor’s account.

 

- Centralized Database

    The system will consist of a centralized database that can be accessed by each authorised hospital in the city. This centralized system will consist of all the past records of the patient, thus enabling report sharing among patient and doctor with ease.

 

- Secured Access and Encrypted Data

    The reports pertaining to a patient can only be view by a doctor if patient has given the necessary authorization to the doctor. The authorization stays for a minimum of 7 days after which the patient can either revoke access or can extend the period of authorization according to the need of his/her treatment. A patient can also share his/her reports with his/her relatives.

 

- Paperless Services

    The entire system is paperless right from making appointments to getting prescription from the doctor. With this system we are trying to eliminate the need of paper at each stage.

### Future prospective

 - Aadhar integration with biometrics

Currently a user requires an email id to register with our platform. A secure system can be incorporated in future which will enable the user to register with their adhaar numbers and the users who do not have smart phones can be provided with all facilities via sms.  

 - Nationwide expansion 

With the active registration system based on adhaar in the future, the current centralized system can be expanded to reach even the most remote areas of the country. The areas like north east where people have to travel far for medical treatment will be most benefited by this platform.


### Platform and Api used

We are building this web application using Node.JS which also incudes framework like Express.Js and mongo db is used for database management.

For controlling the versions of this project, we are using git.

React (a javascript library) is being used for building user interfaces.

For building and managing the models based on machine learning we are using Python.

For data mining and data analysis we are using scikit-learn(a machine learning library for python).


